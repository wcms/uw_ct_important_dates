<?php

/**
 * @file
 * uw_ct_important_dates.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_important_dates_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_important_dates_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_important_dates_node_info() {
  $items = array(
    'important_dates' => array(
      'name' => t('Important Dates'),
      'base' => 'node_content',
      'description' => t('Important Dates'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
