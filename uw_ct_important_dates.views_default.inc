<?php

/**
 * @file
 * uw_ct_important_dates.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function uw_ct_important_dates_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'dates_for_important_date_item';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Dates for Important Date Item';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['pure_distinct'] = TRUE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['grouping'] = array(
    0 => array(
      'field' => 'field_audience',
      'rendered' => 1,
      'rendered_strip' => 0,
    ),
  );
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['row_options']['hide_empty'] = TRUE;
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_important_date']['id'] = 'field_important_date';
  $handler->display->display_options['fields']['field_important_date']['table'] = 'field_data_field_important_date';
  $handler->display->display_options['fields']['field_important_date']['field'] = 'field_important_date';
  $handler->display->display_options['fields']['field_important_date']['label'] = '';
  $handler->display->display_options['fields']['field_important_date']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_important_date']['settings'] = array(
    'format_type' => 'long',
    'fromto' => 'both',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['field_important_date']['group_column'] = 'entity_id';
  $handler->display->display_options['fields']['field_important_date']['group_columns'] = array(
    'value' => 'value',
    'value2' => 'value2',
  );
  $handler->display->display_options['fields']['field_important_date']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_important_date']['separator'] = '     and,';
  /* Field: Content: Audience */
  $handler->display->display_options['fields']['field_audience']['id'] = 'field_audience';
  $handler->display->display_options['fields']['field_audience']['table'] = 'field_data_field_audience';
  $handler->display->display_options['fields']['field_audience']['field'] = 'field_audience';
  $handler->display->display_options['fields']['field_audience']['label'] = '';
  $handler->display->display_options['fields']['field_audience']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_audience']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_audience']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_audience']['group_rows'] = FALSE;
  $handler->display->display_options['fields']['field_audience']['delta_offset'] = '0';
  /* Sort criterion: Content: Date -  start date (field_important_date) */
  $handler->display->display_options['sorts']['field_important_date_value']['id'] = 'field_important_date_value';
  $handler->display->display_options['sorts']['field_important_date_value']['table'] = 'field_data_field_important_date';
  $handler->display->display_options['sorts']['field_important_date_value']['field'] = 'field_important_date_value';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Contextual filter: Content: Description (field_description) */
  $handler->display->display_options['arguments']['field_description_tid']['id'] = 'field_description_tid';
  $handler->display->display_options['arguments']['field_description_tid']['table'] = 'field_data_field_description';
  $handler->display->display_options['arguments']['field_description_tid']['field'] = 'field_description_tid';
  $handler->display->display_options['arguments']['field_description_tid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_description_tid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_description_tid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_description_tid']['summary_options']['items_per_page'] = '25';
  /* Contextual filter: Content: Term (field_important_date_term) */
  $handler->display->display_options['arguments']['field_important_date_term_tid_1']['id'] = 'field_important_date_term_tid_1';
  $handler->display->display_options['arguments']['field_important_date_term_tid_1']['table'] = 'field_data_field_important_date_term';
  $handler->display->display_options['arguments']['field_important_date_term_tid_1']['field'] = 'field_important_date_term_tid';
  $handler->display->display_options['arguments']['field_important_date_term_tid_1']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_important_date_term_tid_1']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_important_date_term_tid_1']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_important_date_term_tid_1']['summary_options']['items_per_page'] = '25';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'important_dates' => 'important_dates',
  );

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block');

  /* Display: Feed */
  $handler = $view->new_display('feed', 'Feed', 'feed_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'views_ical';
  $handler->display->display_options['row_plugin'] = 'views_ical';
  $handler->display->display_options['path'] = 'important-dates/ical';
  $export['dates_for_important_date_item'] = $view;

  $view = new view();
  $view->name = 'important_dates_ical_feed';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Important Dates iCal feed';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Content: Description */
  $handler->display->display_options['fields']['field_description']['id'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['table'] = 'field_data_field_description';
  $handler->display->display_options['fields']['field_description']['field'] = 'field_description';
  $handler->display->display_options['fields']['field_description']['label'] = 'Summary';
  $handler->display->display_options['fields']['field_description']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Audience */
  $handler->display->display_options['fields']['field_audience']['id'] = 'field_audience';
  $handler->display->display_options['fields']['field_audience']['table'] = 'field_data_field_audience';
  $handler->display->display_options['fields']['field_audience']['field'] = 'field_audience';
  $handler->display->display_options['fields']['field_audience']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_audience']['delta_offset'] = '0';
  /* Field: Content: Term */
  $handler->display->display_options['fields']['field_important_date_term']['id'] = 'field_important_date_term';
  $handler->display->display_options['fields']['field_important_date_term']['table'] = 'field_data_field_important_date_term';
  $handler->display->display_options['fields']['field_important_date_term']['field'] = 'field_important_date_term';
  $handler->display->display_options['fields']['field_important_date_term']['label'] = 'Description';
  $handler->display->display_options['fields']['field_important_date_term']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_important_date_term']['alter']['text'] = '[field_audience] - [field_important_date_term]';
  $handler->display->display_options['fields']['field_important_date_term']['type'] = 'taxonomy_term_reference_plain';
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_important_date']['id'] = 'field_important_date';
  $handler->display->display_options['fields']['field_important_date']['table'] = 'field_data_field_important_date';
  $handler->display->display_options['fields']['field_important_date']['field'] = 'field_important_date';
  $handler->display->display_options['fields']['field_important_date']['label'] = 'DTSTART';
  $handler->display->display_options['fields']['field_important_date']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_important_date']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_important_date']['settings'] = array(
    'format_type' => 'ical',
    'fromto' => 'value',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['field_important_date']['delta_offset'] = '0';
  /* Field: Content: Date */
  $handler->display->display_options['fields']['field_important_date_1']['id'] = 'field_important_date_1';
  $handler->display->display_options['fields']['field_important_date_1']['table'] = 'field_data_field_important_date';
  $handler->display->display_options['fields']['field_important_date_1']['field'] = 'field_important_date';
  $handler->display->display_options['fields']['field_important_date_1']['label'] = 'DTEND';
  $handler->display->display_options['fields']['field_important_date_1']['alter']['strip_tags'] = TRUE;
  $handler->display->display_options['fields']['field_important_date_1']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_important_date_1']['settings'] = array(
    'format_type' => 'ical',
    'fromto' => 'value2',
    'multiple_number' => '',
    'multiple_from' => '',
    'multiple_to' => '',
    'show_repeat_rule' => 'show',
  );
  $handler->display->display_options['fields']['field_important_date_1']['delta_offset'] = '0';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'important_dates' => 'important_dates',
  );

  /* Display: Feed */
  $handler = $view->new_display('feed', 'Feed', 'feed_1');
  $handler->display->display_options['pager']['type'] = 'some';
  $handler->display->display_options['style_plugin'] = 'views_ical';
  $handler->display->display_options['row_plugin'] = 'views_ical';
  $handler->display->display_options['path'] = 'important-dates/feeds/ical';

  /* Display: Block */
  $handler = $view->new_display('block', 'Block', 'block_1');
  $export['important_dates_ical_feed'] = $view;

  $view = new view();
  $view->name = 'tax_important_date';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'taxonomy_term_data';
  $view->human_name = 'Important Dates';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'date_tax_test';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['group_by'] = TRUE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'name' => 'name',
    'draggableviews' => 'draggableviews',
    'description' => 'description',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'name' => array(
      'sortable' => 0,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'draggableviews' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'description' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Field: Taxonomy term: Term ID */
  $handler->display->display_options['fields']['tid']['id'] = 'tid';
  $handler->display->display_options['fields']['tid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['tid']['field'] = 'tid';
  $handler->display->display_options['fields']['tid']['exclude'] = TRUE;
  /* Field: Taxonomy term: Term description */
  $handler->display->display_options['fields']['description']['id'] = 'description';
  $handler->display->display_options['fields']['description']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['fields']['description']['field'] = 'description';
  $handler->display->display_options['fields']['description']['label'] = 'Term and Deadlines';
  /* Field: Global: View */
  $handler->display->display_options['fields']['view']['id'] = 'view';
  $handler->display->display_options['fields']['view']['table'] = 'views';
  $handler->display->display_options['fields']['view']['field'] = 'view';
  $handler->display->display_options['fields']['view']['label'] = 'Fall 2013';
  $handler->display->display_options['fields']['view']['view'] = 'dates_for_important_date_item';
  $handler->display->display_options['fields']['view']['display'] = 'block';
  $handler->display->display_options['fields']['view']['arguments'] = '[%tid]/130';
  /* Field: Global: View */
  $handler->display->display_options['fields']['view_1']['id'] = 'view_1';
  $handler->display->display_options['fields']['view_1']['table'] = 'views';
  $handler->display->display_options['fields']['view_1']['field'] = 'view';
  $handler->display->display_options['fields']['view_1']['label'] = 'Winter 2014';
  $handler->display->display_options['fields']['view_1']['view'] = 'dates_for_important_date_item';
  $handler->display->display_options['fields']['view_1']['display'] = 'block';
  $handler->display->display_options['fields']['view_1']['arguments'] = '[%tid]/131';
  /* Field: Global: View */
  $handler->display->display_options['fields']['view_2']['id'] = 'view_2';
  $handler->display->display_options['fields']['view_2']['table'] = 'views';
  $handler->display->display_options['fields']['view_2']['field'] = 'view';
  $handler->display->display_options['fields']['view_2']['label'] = 'Spring 2014';
  $handler->display->display_options['fields']['view_2']['view'] = 'dates_for_important_date_item';
  $handler->display->display_options['fields']['view_2']['display'] = 'block';
  $handler->display->display_options['fields']['view_2']['arguments'] = '[%tid]/132';
  /* Filter criterion: Taxonomy term: Vocabulary */
  $handler->display->display_options['filters']['vid']['id'] = 'vid';
  $handler->display->display_options['filters']['vid']['table'] = 'taxonomy_term_data';
  $handler->display->display_options['filters']['vid']['field'] = 'vid';
  $handler->display->display_options['filters']['vid']['value'] = array(
    14 => '14',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'important-dates';
  $export['tax_important_date'] = $view;

  return $export;
}
